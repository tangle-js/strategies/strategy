const Validator = require('is-my-json-valid')

const Schema = require('./schema')
const Concat = require('./concat')
const { Merge, IsConflict, IsValidMerge } = require('./merge')
const isValidStrategy = require('./is-valid-strategy')
const map = require('./map')

class Strategy {
  static isValid (strategy) {
    const result = isValidStrategy(strategy)
    Strategy.isValid.error = isValidStrategy.error
    return result
  }

  static isValidComposition (composition) {
    const errors = []
    Object.entries(composition).forEach(([name, strategy]) => {
      if (!isValidStrategy(strategy, name)) {
        errors.push(`field "${name}" has invalid strategy`)
      }
    })
    if (errors.length) {
      Strategy.isValidComposition.error = new Error([
        'Invalid composition:',
        ...errors
      ].join('\n - '))

      return false
    }

    Strategy.isValidComposition.error = null
    return true
  }

  constructor (composition) {
    Strategy.isValidComposition(composition)

    this._composition = composition
    this.schema = Schema(composition)

    this.isValid = Validator(this.schema, { verbose: true })
    this.concat = Concat(composition)
    this.mapFromInput = map.FromInput(composition) // input => T
    this.mapToOutput = map.ToOutput(composition) //   T => output
    this.mapToPure = map.ToPure(composition) //       t => T
    this.merge = Merge(composition)
    this.isConflict = IsConflict(composition)
    this.isValidMerge = IsValidMerge(composition)
  }

  /* Methods */
  identity () { return this.mapToPure({}) }
  // isValid
  // concat
  // mapFromInput
  // mapToOutput
  // mapToPure
  // merge
  // isConflict
  // isValidMerge

  /* Getters */
  get fields () { return Object.keys(this._composition).sort() }
  // schema
}

module.exports = Strategy
