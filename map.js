const isEqual = require('lodash.isequal')

module.exports = {
  FromInput (composition) {
    return function mapTransformFromInput (input, currentTips) {
      const T = {}
      // For each field in input, map it to transform using mapFromInput
      Object.entries(input).forEach(([field, inputForField]) => {
        const currentTipsForField = currentTips
          .map(tip => tip[field])
          .filter(Boolean)

        const map = composition[field].mapFromInput || noop
        T[field] = map(inputForField, currentTipsForField)
      })
      return T
    }
  },

  ToOutput (composition) {
    // composition is an Object made up of several strategy

    return function mapTransformToOutput (T) {
      // T is an Object made up of transformations

      const result = {}

      Object.entries(composition)
        .forEach(([field, strategy]) => {
          const { mapToOutput, identity } = strategy

          if (field in T && !isEqual(T[field], identity())) {
            result[field] = mapToOutput(T[field])
          } else {
            result[field] = mapToOutput(identity())
          }
        })

      return result
    }
  },

  ToPure (composition) {
    return function mapTransformToPureTransform (T) {
      // take an Object T, which may have supurflous or missing transformation fields
      // and creates from it a clean + explicit transformation, fullT

      const fullT = {}

      Object.entries(composition).forEach(([field, strategy]) => {
        fullT[field] = T[field] || strategy.identity()
      })

      return fullT
    }
  }
}

function noop (T) { return T }
