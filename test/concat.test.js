const test = require('tape')
const Strategy = require('../')
const { Overwrite, SimpleSet } = require('./strategies')

test('strategy.concat', t => {
  const strategy = new Strategy({
    title: Overwrite(),
    attendees: SimpleSet()
  })

  t.deepEqual(
    strategy.concat(
      {
        title: { set: 'lunch with friends' },
        attendees: { mix: 1 }
      },
      {
        attendees: { mix: -1, luandro: 1, ben: 1 }
      }
    ),
    {
      title: { set: 'lunch with friends' },
      attendees: { luandro: 1, ben: 1 }
    },
    'basic composition'
  )

  const { concat } = strategy
  t.deepEqual(
    concat(
      {
        title: { set: 'lunch with friends' }
      },
      {
        title: { set: 'lunch with mes amis' }
      }
    ),
    {
      title: { set: 'lunch with mes amis' }
    },
    'fields not mentioned are not added'
  )

  t.end()
})
