const test = require('tape')
const Strategy = require('../')
const { Overwrite, SimpleSet } = require('./strategies')
const { buildGraph } = require('@tangle/test')

test('strategy.isConflict', t => {
  const strategy = new Strategy({
    title: Overwrite(),
    attendees: SimpleSet()
  })
  const mergeNodeGraph = buildGraph(`
    A-->B-->D
    A-->C-->D
  `)
  t.equal(strategy.isConflict(mergeNodeGraph, ['B', 'C']), false, 'merge identities')

  const B = mergeNodeGraph.getNode('B')
  B.data.title = { set: 'dog' }
  B.data.attendees = { a: 1 }
  t.equal(strategy.isConflict(mergeNodeGraph, ['B', 'C']), false, 'values on single branch')

  const C = mergeNodeGraph.getNode('C')
  C.data.attendees = { b: 1 }
  t.equal(strategy.isConflict(mergeNodeGraph, ['B', 'C']), false, 'simpleset has no conflict')

  C.data.title = { set: 'cat' }
  t.equal(strategy.isConflict(mergeNodeGraph, ['B', 'C']), true, 'title set in both branches is conflict')

  t.end()
})
