const test = require('tape')
const Strategy = require('../')
const { Overwrite, SimpleSet } = require('./strategies')

test('strategy (basic)', t => {
  const strategy = new Strategy({
    title: Overwrite(),
    attendees: SimpleSet()
  })

  const T1 = {
    title: { set: 'brunch' },
    attendees: { mix: 1, alanna: 1 }
  }
  const T2 = {
    title: { set: 'Brunch at Mixs' },
    attendees: { alanna: -1, ben: 1 }
  }
  const Tbroke = {
    title: { set: 'thing' },
    attendees: 'woops'
  }

  t.false(strategy.isValid(Tbroke), 'is not Valid')
  t.match(strategy.isValid.error, /data.attendees is the wrong type/)

  t.true(strategy.isValid({}), 'isValid (empty)')
  t.true(strategy.isValid(strategy.identity()), 'isValid (identity)')
  t.true(strategy.isValid(T1), 'isValid')
  t.true(strategy.isValid(T2), 'isValid')

  t.equal(strategy.isValid.error, '', 'error')

  const T3 = strategy.concat(T1, T2)

  t.deepEqual(
    T3,
    {
      title: { set: 'Brunch at Mixs' },
      attendees: { mix: 1, ben: 1 }
    },
    'concat'
  )

  t.deepEqual(
    strategy.mapToOutput(T3),
    {
      title: 'Brunch at Mixs',
      attendees: ['ben', 'mix']
    },
    'mapToOutput'
  )

  t.deepEqual(
    strategy.mapToPure({
      title: { set: 'dog' },
      recps: ['@mix']
    }),
    {
      title: { set: 'dog' },
      attendees: {}
    },
    'mapToPure'
  )

  const { mapToPure } = strategy
  t.deepEqual(
    mapToPure({
      title: { set: 'dog' },
      recps: ['@mix']
    }),
    {
      title: { set: 'dog' },
      attendees: {}
    },
    'mapToPure'
  )

  t.end()
})
