const test = require('tape')
const { ToOutput } = require('../map')
const { Overwrite, SimpleSet } = require('./strategies')

test('strategy.mapToOutput', t => {
  const mapToOutput = ToOutput({
    title: Overwrite(),
    attendees: SimpleSet()
  })

  t.deepEqual(
    mapToOutput({
      title: { set: 'lunch with friends' },
      attendees: { mix: -1, luandro: 2, ben: 1, alanna: 0 }
    }),
    {
      title: 'lunch with friends',
      attendees: ['ben', 'luandro']
    },
    'basic mapToOutput'
  )

  t.deepEqual(
    mapToOutput({}),
    {
      title: null,
      attendees: []
    },
    'fields not mentioned are reified to their reified(identity)'
  )

  t.end()
})
