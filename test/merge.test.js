const test = require('tape')
const Strategy = require('../')
const { Overwrite, SimpleSet } = require('./strategies')
const { buildGraph } = require('@tangle/test')

test('strategy.merge', t => {
  const strategy = new Strategy({
    title: Overwrite(),
    attendees: SimpleSet()
  })
  const mergeNodeGraph = buildGraph(`
    A-->B-->D
    A-->C-->D
  `)
  const mergeNode = mergeNodeGraph.getNode('D')

  t.deepEqual(strategy.merge(mergeNodeGraph, mergeNode),
    { title: {}, attendees: {} }, 'merge identities')

  const B = mergeNodeGraph.getNode('B')
  B.data.title = { set: 'dog' }
  B.data.attendees = { a: 1 }
  t.deepEqual(strategy.merge(mergeNodeGraph, mergeNode),
    { title: { set: 'dog' }, attendees: { a: 1 } }, 'identity merge is fine for non-conflict')

  const C = mergeNodeGraph.getNode('C')
  C.data.attendees = { b: 1 }
  t.deepEqual(strategy.merge(mergeNodeGraph, mergeNode),
    { title: { set: 'dog' }, attendees: { a: 1, b: 1 } }, 'merge simpleset')

  C.data.title = { set: 'cat' }
  t.throws(() => strategy.merge(mergeNodeGraph, mergeNode), /Field title is causing a conflict/,
    'merge does not resolve conflict')

  mergeNode.data.title = { set: 'mouse' }
  mergeNode.data.attendees = { b: 1 }
  t.deepEqual(strategy.merge(mergeNodeGraph, mergeNode),
    { title: { set: 'mouse' }, attendees: { a: 1, b: 2 } }, 'nonidentity merge node')

  t.end()
})
