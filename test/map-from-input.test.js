const test = require('tape')
const { Overwrite, SimpleSet, ComplexSet } = require('./strategies')

const Strategy = require('../')

test('strategy.mapFromInput', t => {
  const strategy = new Strategy({
    title: Overwrite(),
    attendees: SimpleSet(),
    authors: ComplexSet()
  })

  const currentT = {
    title: { set: 'party' },
    attendees: {
      dale: 2
    },
    authors: {
      mix: { 50: 1 }
    }
  }
  const human = {
    title: 'beach party',
    attendees: {
      add: ['trevor'],
      remove: ['dale']
    },
    authors: {
      add: [{ id: 'travis', seq: 2 }],
      remove: [{ id: 'mix', seq: 5000 }]
    }
  }

  t.deepEqual(
    strategy.mapFromInput(human, [currentT]),
    {
      title: { set: 'beach party' },
      attendees: {
        trevor: 1,
        dale: -3
      },
      authors: {
        travis: { 2: 1 },
        mix: { 5000: -1 }
      }
    }
  )

  const currentTips = [{
    title: { set: 'party' },
    attendees: {
      dale: 2
    },
    authors: {
      mix: { 50: 1 }
    }
  },
  {
    title: { set: 'holiday' }
  }
  ]

  t.deepEqual(
    strategy.mapFromInput(human, currentTips),
    {
      title: { set: 'beach party' },
      attendees: {
        trevor: 1,
        dale: -3
      },
      authors: {
        travis: { 2: 1 },
        mix: { 5000: -1 }
      }
    }
  )

  t.end()
})
