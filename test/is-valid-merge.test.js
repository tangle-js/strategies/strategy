const test = require('tape')
const Strategy = require('../')
const { Overwrite, SimpleSet } = require('./strategies')
const { buildGraph } = require('@tangle/test')

test('strategy.isValidMerge', t => {
  const strategy = new Strategy({
    title: Overwrite(),
    attendees: SimpleSet()
  })
  const mergeNodeGraph = buildGraph(`
    A-->B-->D
    A-->C-->D
  `)
  const mergeNode = mergeNodeGraph.getNode('D')
  t.equal(strategy.isValidMerge(mergeNodeGraph, mergeNode), true, 'merge identities')

  const B = mergeNodeGraph.getNode('B')
  B.data.title = { set: 'dog' }
  B.data.attendees = { a: 1 }
  t.equal(strategy.isValidMerge(mergeNodeGraph, mergeNode), true, 'identity merge is fine for non-conflict')

  const C = mergeNodeGraph.getNode('C')
  C.data.title = { set: 'cat' }
  t.equal(strategy.isValidMerge(mergeNodeGraph, mergeNode), false, 'merge that does not resolve conflict is invalid')
  t.match(strategy.isValidMerge.error.message, /Merge conflict.*title/)
  t.match(strategy.isValidMerge.errors[0].message, /title/)
  t.deepEqual(strategy.isValidMerge.fields, ['title'])

  mergeNode.data.title = { set: 'mouse' }
  mergeNode.data.attendees = { b: 1 }
  t.equal(strategy.isValidMerge(mergeNodeGraph, mergeNode), true, 'nonidentity merge node')
  t.deepEqual(strategy.isValidMerge.error, null)
  t.deepEqual(strategy.isValidMerge.errors, [])
  t.deepEqual(strategy.isValidMerge.fields, [])

  t.end()
})
