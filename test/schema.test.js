const test = require('tape')
const overwrite = require('@tangle/overwrite')()
const simpleSet = require('@tangle/simple-set')()
const Validator = require('is-my-json-valid')

const Schema = require('../schema')

test('strategy.schema', t => {
  const schema = Schema({
    title: overwrite,
    attendees: simpleSet
  })

  t.deepEqual(
    schema,
    {
      type: 'object',
      properties: {
        title: overwrite.schema,
        attendees: simpleSet.schema
      },
      additionalProperties: false
    },
    'basic schema'
  )

  const isValid = Validator(schema)

  const Ts = [
    {
      title: { set: 'boop' }
    },
    {
      attendees: {
        mixmix: 2
      }
    }
  ]
  Ts.forEach(T => {
    t.true(isValid(T), `isValid : ${JSON.stringify(T)}`)
    if (isValid.error) console.log(isValid.error)
  })

  t.end()
})
