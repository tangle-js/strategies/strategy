module.exports = function Concat (composition) {
  // composition is an Object made up of several strategy

  return function (a, b) {
    const c = {}

    Object.entries(composition).forEach(([field, strategy]) => {
      if (!(field in a) && !(field in b)) return // eslint-disable-line
      else if (!(field in a)) c[field] = b[field]
      else if (!(field in b)) c[field] = a[field]
      else {
        c[field] = strategy.concat(a[field], b[field])
      }
    })

    return c
  }
}
